import kanoahTestsPythonClient as kanoahTests

# Search test cases on project "TIS" and folder "/REST API"
foundTestCases = kanoahTests.searchTestCases('TIS', 'folder = "/REST API"')
print [testCase['key'] for testCase in foundTestCases]

# Create a test run with found test cases
testRun = kanoahTests.createTestRunWithStatus('My Test Run Python', 'TIS', [
		{'testCaseKey': testCase['key'], 'status': 'Pass'} for testCase in foundTestCases
	])
print testRun

# Update first test case of test run to set status "Fail"
kanoahTests.updateTestRun(testRun['key'], foundTestCases[0]['key'], "Fail")