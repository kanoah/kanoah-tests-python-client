import progressbar
import kanoahTestsPythonClient as kanoahTests

testCaseCount = 1000
stepCount = 15
testRunCount = 20
projectKey = 'DEF'

barTestCases = progressbar.ProgressBar()
barTestRuns = progressbar.ProgressBar()
testCaseKeys = [kanoahTests.createTestCase('My Test Case Python ' + `i`, projectKey, stepCount)['key'] for i in barTestCases(range(testCaseCount))]
[kanoahTests.createTestRun('My Test Run Python ' + `i`, projectKey, testCaseKeys) for i in barTestRuns(range(testRunCount))]