Usage examples of Kanoah Tests REST API. Feel free to contribute :)

### How do I get set up? ###

* Edit file config.py with your credentials and JIRA url
* Run some of the example files using python: 
```
python exampleCoverage.py
```