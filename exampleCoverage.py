import kanoahTestsPythonClient as kanoahTests

someTestCase = kanoahTests.getTestCase('TIS-T41439')
print(someTestCase['key'])

issueCoverage = kanoahTests.getIssueCoverage('TIS-38')
print([testCase['key'] for testCase in issueCoverage])

kanoahTests.createTestRun('Regression for TIS-38', 'TIS', [testCase['key'] for testCase in issueCoverage])