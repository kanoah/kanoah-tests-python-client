import requests
from requests.auth import HTTPBasicAuth
import config
import urllib

API_BASE_URL = config.base_url
user = {'username': config.user, 'password': config.password}

# Send a GET request to the API
def sendGetRequest(endpoint):
    response = requests.get(API_BASE_URL + endpoint, auth=HTTPBasicAuth(user['username'], user['password']))
    if response.status_code != 200 and response.status_code != 201:
        raise Exception('GET {}{}, statusCode: {}, body: {}'.format(API_BASE_URL, endpoint, response.status_code, response.text))
    return response

# Send a POST request with multipart to the API
def sendPostRequestWithMultipart(endpoint, files):
    response = requests.post(API_BASE_URL + endpoint, auth=HTTPBasicAuth(user['username'], user['password']), files=files)
    if response.status_code != 201 and response.status_code != 200:
        raise Exception('POST {}{}, statusCode: {}, body: {}'.format(API_BASE_URL, endpoint, response.status_code, response.text))
    return response

# Send a POST request to the API
def sendPostRequest(endpoint, json):
    response = requests.post(API_BASE_URL + endpoint, auth=HTTPBasicAuth(user['username'], user['password']), json=json)
    if response.status_code != 201 and response.status_code != 200:
        raise Exception('POST {}{}, statusCode: {}, body: {}'.format(API_BASE_URL, endpoint, response.status_code, response.text))
    return response

# Send a PUT request to the API
def sendPutRequest(endpoint, json):
    response = requests.put(API_BASE_URL + endpoint, auth=HTTPBasicAuth(user['username'], user['password']), json=json)
    if response.status_code != 201 and response.status_code != 200:
        raise Exception('PUT {}{}, statusCode: {}, body: {}'.format(API_BASE_URL, endpoint, response.status_code, response.text))
    return response

# Gets a test case by key, using 'GET /testcase/{testCaseKey}'
def getTestCase(testCaseKey):
    endpoint = 'testcase/{}'.format(testCaseKey)
    return sendGetRequest(endpoint).json()

# Search test cases, using 'GET /testcase/search?query={query}'
def searchTestCases(projectKey, filter):
    endpoint = 'testcase/search/?' + urllib.urlencode({'query': 'projectKey = "{}" AND {}'.format(projectKey, filter)})
    return sendGetRequest(endpoint).json()

# Gets all test cases that covers an issue with the given key, using 'GET /issuelink/{issueKey}/testcases'
def getIssueCoverage(issueKey):
    endpoint = 'issuelink/{}/testcases'.format(issueKey)
    return sendGetRequest(endpoint).json()

# Creates a test result, using 'POST /testresult'
def createTestResult(projectKey, testCaseKey, status, environment):
    endpoint = 'testresult'
    return sendPostRequest(endpoint, {'projectKey': projectKey, 'testCaseKey': testCaseKey, 'status': status, 'environment': environment}).json()

# Creates a test run, using 'POST /testrun'
def createTestRun(name, projectKey, testCaseKeys):
    endpoint = 'testrun'
    items = [{'testCaseKey': testCaseKey} for testCaseKey in testCaseKeys]
    return createTestRunWithStatus(name, projectKey, items)

# Updates a test run, using 'PUTT /testrun/{testRunKey}/testcase/{testCaseKey}/testresult'
def updateTestRun(testRunKey, testCaseKey, status):
    endpoint = 'testrun/{}/testcase/{}/testresult'.format(testRunKey, testCaseKey)
    return sendPutRequest(endpoint, {'status': status}).json()

# Creates a test run, using 'POST /testrun'
def createTestRunWithStatus(name, projectKey, items):
    endpoint = 'testrun'
    return sendPostRequest(endpoint, {'name': name, 'projectKey': projectKey, 'items': items}).json()

# Creates a test case with random steps, using 'POST /testcase'
def createTestCase(name, projectKey, stepCount):
    endpoint = 'testcase'
    return sendPostRequest(endpoint, {
            'name': name,
            'projectKey': projectKey,
            'testScript': {
                'type': 'STEP_BY_STEP',
                'steps': [{'description': 'Description ' + (i + 1), 'expectedResult': 'Expected Result ' + (i + 1)} for i in range(stepCount)]
              }
        }).json()

# Attaches a file to a test result
def attachFile(testResultId, fileName):
    endpoint = 'testresult/{}/attachment'.format(testResultId)
    files = {'file': open(fileName)}
    sendPostRequestWithMultipart(endpoint, files)
