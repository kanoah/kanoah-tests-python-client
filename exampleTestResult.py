import kanoahTestsPythonClient as kanoahTests

# Search test cases on project "TIS" and folder "/REST API"
foundTestCases = kanoahTests.searchTestCases('TIS', 'folder = "/REST API"')
print [testCase['key'] for testCase in foundTestCases]

# Create a test result for each test case, with result "Fail"
for i in range(0, len(foundTestCases)):
	kanoahTests.createTestResult('TIS', foundTestCases[i]['key'], 'Fail', None)