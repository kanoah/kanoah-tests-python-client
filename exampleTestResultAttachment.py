import kanoahTestsPythonClient as kanoahTests

# Search test cases on project "TIS" and folder "/REST API"
foundTestCases = kanoahTests.searchTestCases('TIS', 'folder = "/REST API"')
print [testCase['key'] for testCase in foundTestCases]

# Create a test run with found test cases
testRun = kanoahTests.createTestRunWithStatus('My Test Run Python', 'TIS', [
		{'testCaseKey': testCase['key'], 'status': 'Pass'} for testCase in foundTestCases
	])
print testRun

# Update a test result of the created test run
testResult = kanoahTests.updateTestRun(testRun['key'], foundTestCases[0]['key'], "Fail")
print testResult

# Attach a file on the test result
kanoahTests.attachFile(testResult['id'], 'fileToUpload.png')